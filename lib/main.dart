import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather/bloc/bloc_observer.dart';
import 'package:weather/bloc/localization/localization_bloc.dart';
import 'package:weather/bloc/localization/localization_state.dart';
import 'package:weather/bloc/theme/theme_bloc.dart';
import 'package:weather/bloc/theme/theme_state.dart';
import 'package:weather/di/singleton.dart';
import 'package:weather/navigator/route_generate.dart';
import 'package:weather/navigator/screens_name.dart';
import 'package:weather/utils/theme.dart';

void main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // set observer for Bloc
  await ScreenUtil.ensureScreenSize();
  Bloc.observer = AppBlocObserver();
  runApp(
    MultiBlocProvider(
      providers: [
        // BlocProvider<ThemeBloc>(
        //   create: (BuildContext context) => ThemeBloc(),
        // ),
        BlocProvider<LocalizationBloc>(
          create: (BuildContext context) => LocalizationBloc(),
        ),
      ],
      child: const MyApp(
        key: Key("Weather"),
      ),
    ),
  );
}

// create MyApp StatefulWidget
class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with AfterLayoutMixin<MyApp> {
  Locale _locale = Locale('vi');

  @override
  void initState() {
    super.initState();
    // setup singleton
    Singleton.setupLocators();
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    // hide splash screen
    FlutterNativeSplash.remove();
  }

  @override
  void dispose() {
    super.dispose();
    // dispose singleton
    Singleton.dispose();
  }

  void _changeLocalization(BuildContext context, LocalizationState state) {
    if (state is LocalizationChangedState) {
      setState(() {
        _locale = state.locale;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return BlocListener<LocalizationBloc, LocalizationState>(
      listener: _changeLocalization,
      child: ThemeProvider(
        initTheme: getThemeData(Brightness.light),
        builder: (context, currentTheme) {
          return MaterialApp(
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            locale: _locale,
            debugShowCheckedModeBanner: false,
            onGenerateRoute: routeGenerate,
            theme: currentTheme,
            initialRoute: ScreensName.home,
            navigatorKey: Singleton.navigatorService().navigatorKey,
          );
        },
      ),
    );
  }
}
