import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather/utils/widgets/row_text_icon.dart';

class ItemSearchList extends StatelessWidget {
  final String title;
  final Function onPressed;

  const ItemSearchList({super.key, required this.title, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    final bool isDarkMode = Theme.of(context).brightness == Brightness.dark;
    return MaterialButton(
      onPressed: () => onPressed(),
      padding: EdgeInsets.zero,
      child: Container(
        padding: EdgeInsets.all(15),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: isDarkMode ? Colors.blueGrey.shade900 : Colors.blueGrey.shade50,
          borderRadius: BorderRadius.circular(10),
        ),
        child: RowTextIcon(
          title: title,
          maxLines: 2,
          iconWidget: Icon(
            Icons.location_pin,
            color: Theme.of(context).colorScheme.onBackground,
            size: 18.sp,
          ),
          iconPosition: IconPosition.left,
          textStyle: Theme.of(context).textTheme.bodyMedium,
          iconSize: 20.sp,
        ),
      ),
    );
  }
}
