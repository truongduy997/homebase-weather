import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/bloc/weather/weather_bloc.dart';
import 'package:weather/bloc/weather/weather_event.dart';
import 'package:weather/bloc/weather/weather_state.dart';
import 'package:weather/navigator/screens_name.dart';
import 'package:weather/screens/search/widgets/item_search_list.dart';
import 'package:weather/services/models/search_location_model.dart';
import 'package:weather/utils/functions.dart';
import 'package:weather/utils/language.dart';
import 'package:weather/utils/widgets/search_input_widget.dart';
import 'package:weather/utils/widgets/shimmer_loading.dart';

import 'widgets/header_search.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> with AfterLayoutMixin {
  TextEditingController _searchController = TextEditingController(text: "");
  WeatherBloc _weatherBloc = WeatherBloc();
  FocusNode _myFocusNode = FocusNode();

  @override
  void dispose() {
    _searchController.clear();
    _searchController.dispose();
    _myFocusNode.unfocus();
    _myFocusNode.dispose();
    _weatherBloc.close();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    Timer(Duration(milliseconds: 500), () {
      _myFocusNode.requestFocus();
    });
  }

  void _onChangeText(String text) {
    if (text.isEmpty) {
      _weatherBloc.add(RefreshWeatherEvent());
    } else {
      debounceFunc(() {
        printMessage("_onChangeText ${text}");
        _weatherBloc.add(SearchLocationWeather(text));
      }, 1000);
    }
  }

  void _onPressItemSearch(SearchLocationModel searchLocationModel) {
    // TODO: implement action
    _searchController.clear();
    _myFocusNode.unfocus();
    _weatherBloc.add(GetCurrentWeather(searchLocationModel.lat!, searchLocationModel.lon!));
  }

  void _listener(BuildContext context, WeatherState state) {
    if (state is GetCurrentWeatherStateSuccess) {
      Navigator.of(context).pushNamed(ScreensName.details, arguments: state.currentWeatherModel);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<WeatherBloc, WeatherState>(
      bloc: _weatherBloc,
      listener: _listener,
      child: Hero(
        tag: "search-weather",
        child: Scaffold(
          backgroundColor: Theme.of(context).colorScheme.background,
          body: GestureDetector(
            onTap: () => _myFocusNode.unfocus(),
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(vertical: 20),
              physics: NeverScrollableScrollPhysics(),
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              child: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    HeaderSearch(),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: SearchInputWidget(
                        controller: _searchController,
                        focusNode: _myFocusNode,
                        onChangedText: _onChangeText,
                        placeholderText: Language.shared.searchInput,
                      ),
                    ),
                    BlocBuilder<WeatherBloc, WeatherState>(
                      bloc: _weatherBloc,
                      builder: (context, state) {
                        if (state is SearchLocationWeatherSuccess) {
                          if (state.locations.isEmpty && _searchController.text.isNotEmpty) {
                            return Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.topCenter,
                              child: AutoSizeText(Language.shared.searchEmpty,
                                  style: Theme.of(context).textTheme.bodyLarge),
                            );
                          }
                          return Container(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            height: MediaQuery.of(context).size.height - 200,
                            child: ListView.builder(
                                itemCount: state.locations.length,
                                itemBuilder: (context, index) {
                                  final _name = state.locations[index].name;
                                  final _country = state.locations[index].country;
                                  return ItemSearchList(
                                      title: "${_name} - ${_country}",
                                      onPressed: () => _onPressItemSearch(state.locations[index]));
                                }),
                          );
                        } else if (_searchController.text.isNotEmpty) {
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Material(
                              type: MaterialType.transparency,
                              child: ThemeSwitcher.withTheme(
                                builder: (context, _, theme) {
                                  return ShimmerLoading(
                                      borderRadius: 10,
                                      baseColor: theme.brightness == Brightness.light
                                          ? Colors.blueGrey.shade50
                                          : Colors.blueGrey.shade900,
                                      highlightColor: theme.brightness == Brightness.light
                                          ? Colors.blueGrey.shade200
                                          : Colors.blueGrey.shade600,
                                      height: 50);
                                },
                              ),
                            ),
                          );
                        }
                        return SizedBox();
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
