import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:weather/navigator/screens_name.dart';

class HeaderDetail extends StatelessWidget {
  final String title;

  const HeaderDetail({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
            onPressed: () => Navigator.of(context).popUntil((route) => route.settings.name == ScreensName.home),
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 28,
            )),
        Container(
          constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width - 68, minWidth: MediaQuery.of(context).size.width - 88),
          child: AutoSizeText(
            title,
            textAlign: TextAlign.center,
            maxLines: 1,
            overflow: TextOverflow.clip,
            style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
          ),
        ),
      ],
    );
  }
}
