import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather/services/models/forecast_model.dart';
import 'package:weather/utils/extensions.dart';
import 'package:weather/utils/widgets/row_text_icon.dart';

class ItemListForecast extends StatelessWidget {
  final int indexItem;
  final ForecastDayModel forecastDayModel;
  const ItemListForecast({super.key, required this.forecastDayModel, required this.indexItem});

  @override
  Widget build(BuildContext context) {
    final _conditionText = this.forecastDayModel.day?.condition?.text ?? "";
    final _dateParse = DateTime.parse(this.forecastDayModel.date ?? "");
    final _iconCondition =
        (this.forecastDayModel.day?.condition?.icon ?? "").replaceFirst("//", "https://");
    final _avgTempC = this.forecastDayModel.day?.avgtempC?.toInt() ?? 0;
    final _avgTempCString = _avgTempC.toString() + "\u2103";
    final _avgTempF = this.forecastDayModel.day?.avgtempF?.toInt() ?? 0;
    final __avgTempFString = _avgTempF.toString() + "\u2109";
    String _dayInWeek = _dateParse.getWeekDayString();
    _dayInWeek = this.indexItem == 0 ? "Today" : _dayInWeek;
    _dayInWeek = this.indexItem == 1 ? "Tomorrow" : _dayInWeek;
    return Padding(
      padding: EdgeInsets.only(top: this.indexItem == 0 ? 0 : 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RowTextIcon(
              urlIcon: _iconCondition,
              title: _dayInWeek + " - " + _conditionText,
              iconSize: 28.sp,
            ),
          ),
          Material(
            type: MaterialType.transparency,
            child: AutoSizeText(
              "${_avgTempCString} / ${__avgTempFString}",
              textAlign: TextAlign.end,
              style: Theme.of(context).textTheme.bodySmall?.copyWith(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
