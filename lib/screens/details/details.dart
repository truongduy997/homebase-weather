import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather/bloc/weather/weather_bloc.dart';
import 'package:weather/bloc/weather/weather_event.dart';
import 'package:weather/bloc/weather/weather_state.dart';
import 'package:weather/gen/assets.gen.dart';
import 'package:weather/screens/details/widgets/item_list_forecast.dart';
import 'package:weather/services/models/current_model.dart';
import 'package:weather/services/models/current_weather_model.dart';
import 'package:weather/services/models/forecast_model.dart';
import 'package:weather/services/models/location_model.dart';
import 'package:weather/utils/extensions.dart';
import 'package:weather/utils/functions.dart';
import 'package:weather/utils/hex_color.dart';
import 'package:weather/utils/language.dart';
import 'package:weather/utils/widgets/shimmer_loading.dart';

import 'widgets/header_detail.dart';

class DetailsScreen extends StatefulWidget {
  final CurrentWeatherModel currentWeatherModel;

  DetailsScreen({super.key, required this.currentWeatherModel});

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> with AfterLayoutMixin {
  WeatherBloc _weatherBloc = WeatherBloc();

  @override
  void dispose() {
    // TODO: implement dispose

    _weatherBloc.close();
    super.dispose();
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    _weatherBloc.add(GetForecastWeather(widget.currentWeatherModel.location as LocationModel));
  }

  @override
  Widget build(BuildContext context) {
    // timeDilation = 0.5;
    final CurrentModel? _currentModel = widget.currentWeatherModel.current;
    final LocationModel? _locationModel = widget.currentWeatherModel.location;
    final _titlePosition = _locationModel?.name ?? "";
    final _conditionText = _currentModel?.condition?.text ?? "";
    final _iconCondition = (_currentModel?.condition?.icon ?? "").replaceFirst("//", "https://");
    final _lastUpdate = _currentModel?.lastUpdated ?? "";
    final _tempC = _currentModel?.tempC?.toInt() ?? 0;
    final _tempCString = _tempC.toString() + "\u2103";
    final _tempF = _currentModel?.tempF?.toInt() ?? 0;
    final _tempFString = _tempF.toString() + "\u2109";
    return Hero(
      tag: "detail-screen",
      child: Scaffold(
        body: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  gradient: LinearGradient(begin: Alignment.topLeft, colors: [
                HexColor("405EB9").withOpacity(0.4),
                HexColor("405EB9").withOpacity(0.9)
                // Color(0xff1f005c),
              ])),
              child: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    HeaderDetail(
                      title: _titlePosition,
                    ),
                    SizedBox(height: 100),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Material(
                            type: MaterialType.transparency,
                            child: AutoSizeText("${_tempCString} / ${_tempFString}",
                                style: TextStyle(fontSize: 48.sp, color: Colors.white, fontWeight: FontWeight.bold)),
                          ),
                          RowCondition(
                            iconUrl: _iconCondition,
                            title: _conditionText,
                          ),
                          // SizedBox(
                          //   height: 5,
                          // ),
                          // RowCondition(
                          //   title: Language.shared.lastUpdate + " " + _lastUpdate,
                          //   textStyle: Theme.of(context).textTheme.bodySmall?.copyWith(color: Colors.white60),
                          //   iconWidget: Icon(
                          //     Icons.update,
                          //     color: Colors.white60,
                          //     size: 14.sp,
                          //   ),
                          // ),
                          Expanded(
                            flex: 1,
                            child: SizedBox(),
                          ),
                          Container(
                            alignment: Alignment.bottomCenter,
                            margin: EdgeInsets.symmetric(horizontal: 12),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15), color: HexColor("405EB9").withOpacity(1)),
                            child: BlocBuilder<WeatherBloc, WeatherState>(
                              bloc: _weatherBloc,
                              builder: (context, state) {
                                if (state is GetForecastWeatherSuccess) {
                                  final _currentModel = state.forecastWeatherModel.current;
                                  final _humidity = _currentModel?.humidity ?? 0;
                                  final _windKm = _currentModel?.windKph ?? 0;
                                  final _precipitation = _currentModel?.precipMm ?? 0;
                                  return Padding(
                                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Assets.images.icHumidity.image(width: 48.sp, height: 48.sp),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Material(
                                                type: MaterialType.transparency,
                                                child: AutoSizeText(
                                                  "${_humidity}%",
                                                  textAlign: TextAlign.end,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .labelSmall
                                                      ?.copyWith(color: Colors.white),
                                                ),
                                              )
                                            ],
                                          ),
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Assets.images.icFreezer.image(width: 48.sp, height: 48.sp),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Material(
                                                type: MaterialType.transparency,
                                                child: AutoSizeText(
                                                  "${_precipitation}%",
                                                  textAlign: TextAlign.end,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .labelSmall
                                                      ?.copyWith(color: Colors.white),
                                                ),
                                              )
                                            ],
                                          ),
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Assets.images.icWindy.image(width: 48.sp, height: 48.sp),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Material(
                                                type: MaterialType.transparency,
                                                child: AutoSizeText(
                                                  "${_windKm}%",
                                                  textAlign: TextAlign.end,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .labelSmall
                                                      ?.copyWith(color: Colors.white),
                                                ),
                                              )
                                            ],
                                          )
                                        ]),
                                  );
                                }
                                return ShimmerLoading(
                                  baseColor: HexColor("405EB9").withOpacity(1),
                                  highlightColor: Colors.blueAccent,
                                  height: 140,
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            alignment: Alignment.bottomCenter,
                            margin: EdgeInsets.symmetric(horizontal: 12),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15), color: HexColor("405EB9").withOpacity(1)),
                            child: BlocBuilder<WeatherBloc, WeatherState>(
                              bloc: _weatherBloc,
                              builder: (context, state) {
                                if (state is GetForecastWeatherSuccess) {
                                  final _forecastDay =
                                      state.forecastWeatherModel.forecast?.forecastDay ?? <ForecastDayModel>[];
                                  return Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: _forecastDay.mapIndexed((index, item) {
                                          return ItemListForecast(forecastDayModel: item, indexItem: index);
                                        }).toList()),
                                  );
                                }
                                return ShimmerLoading(
                                  baseColor: HexColor("405EB9").withOpacity(1),
                                  highlightColor: Colors.blueAccent,
                                  height: 260,
                                );
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class RowCondition extends StatelessWidget {
  final String iconUrl;
  final String title;
  final Widget? iconWidget;
  final TextStyle? textStyle;

  const RowCondition({super.key, this.iconUrl = "", required this.title, this.iconWidget, this.textStyle});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        iconWidget ??
            CachedNetworkImage(
              width: 28.sp,
              height: 28.sp,
              imageUrl: iconUrl,
              cacheKey: iconUrl,
              placeholder: (context, url) {
                return SizedBox(
                    width: 16.sp,
                    height: 16.sp,
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ));
              },
              errorWidget: (context, url, error) {
                printErrorMessage("CachedNetworkImage", error);
                return Icon(
                  Icons.error,
                  size: 18.sp,
                  color: Colors.white,
                );
              },
            ),
        SizedBox(
          width: 5,
        ),
        Padding(
          padding: EdgeInsets.only(right: 14.sp),
          child: AutoSizeText(
            title,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: textStyle ?? Theme.of(context).textTheme.labelSmall?.copyWith(color: Colors.white),
          ),
        ),
      ],
    );
  }
}
