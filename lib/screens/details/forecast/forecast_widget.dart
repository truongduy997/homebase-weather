import 'package:flutter/material.dart';
import 'package:weather/services/models/forecast_weather_model.dart';
import 'package:weather/utils/hex_color.dart';

class ForecastWidget extends StatefulWidget {
  final ForecastWeatherModel forecastWeatherModel;
  const ForecastWidget({super.key, required this.forecastWeatherModel});

  @override
  State<ForecastWidget> createState() => _ForecastWidgetState();
}

class _ForecastWidgetState extends State<ForecastWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topLeft, colors: [
      HexColor("405EB9").withOpacity(0.2),
      HexColor("405EB9").withOpacity(0.5)
      // Color(0xff1f005c),
    ])));
  }
}
