import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather/bloc/localization/localization_bloc.dart';
import 'package:weather/bloc/localization/localization_event.dart';
import 'package:weather/bloc/weather/weather_bloc.dart';
import 'package:weather/bloc/weather/weather_event.dart';
import 'package:weather/bloc/weather/weather_state.dart';
import 'package:weather/navigator/screens_name.dart';
import 'package:weather/screens/home/widgets/item_weather_location.dart';
import 'package:weather/utils/functions.dart';
import 'package:weather/utils/geolocator/geolocator.dart';
import 'package:weather/utils/hex_color.dart';
import 'package:weather/utils/language.dart';
import 'package:weather/utils/theme.dart';
import 'package:weather/utils/widgets/search_input_widget.dart';
import 'package:weather/utils/widgets/shimmer_loading.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with AfterLayoutMixin<HomeScreen> {
  WeatherBloc _weatherBloc = WeatherBloc();
  TextEditingController _searchController = TextEditingController(text: "");

  @override
  void initState() {
    getCurrentPosition().then((position) => _weatherBloc.add(GetCurrentWeather(position.latitude, position.longitude)));

    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _weatherBloc.close();
    super.dispose();
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    printMessage("afterFirstLayout");
  }

  void _onRefresh() {
    _weatherBloc.add(RefreshWeatherEvent());
    getCurrentPosition().then((position) => _weatherBloc.add(GetCurrentWeather(position.latitude, position.longitude)));
  }

  void _moveToDetails(BuildContext context, Object? arg) {
    Navigator.of(context).pushNamed(ScreensName.details, arguments: arg);
  }

  void _moveToSearch(BuildContext context) {
    Navigator.of(context).pushNamed(ScreensName.search);
  }

  void _changeTheme(BuildContext context, ThemeSwitcherState switcherState) {
    final theme = Theme.of(context);
    switcherState.changeTheme(
        theme: getThemeData(theme.brightness == Brightness.light ? Brightness.dark : Brightness.light));
  }

  void _changeLanguage(BuildContext context) {
    final _currentLocal = Localizations.localeOf(context);
    LocalizationBloc.of(context).add(ChangeLanguageEvent(Locale(_currentLocal.languageCode == 'vi' ? 'en' : 'vi')));
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final _currentLocal = Localizations.localeOf(context);
    return PopScope(
      canPop: false,
      child: ThemeSwitchingArea(
        child: Scaffold(
          backgroundColor: theme.colorScheme.background,
          floatingActionButton: ThemeSwitcher.switcher(builder: (_, switcher) {
            final _icon = theme.brightness == Brightness.light ? Icons.dark_mode : Icons.light_mode;
            final _bgColor = theme.brightness == Brightness.light ? HexColor("405EB9") : Colors.blueGrey.shade900;
            return Container(
              height: 116,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: 48,
                    height: 48,
                    decoration: BoxDecoration(color: _bgColor, borderRadius: BorderRadius.circular(30)),
                    child: MaterialButton(
                      onPressed: () => _changeLanguage(context),
                      padding: EdgeInsets.zero,
                      child: AutoSizeText(
                        _currentLocal.languageCode.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 12.sp, color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    width: 48,
                    height: 48,
                    decoration: BoxDecoration(color: _bgColor, borderRadius: BorderRadius.circular(30)),
                    child: IconButton(
                      icon: Icon(_icon, size: 24.sp, color: Colors.white),
                      onPressed: () => _changeTheme(context, switcher),
                    ),
                  )
                ],
              ),
            );
          }),
          body: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SafeArea(
                child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AutoSizeText(
                        Language.shared.cities,
                        style: theme.textTheme.labelLarge?.copyWith(fontWeight: FontWeight.w700),
                      ),
                      BlocBuilder<WeatherBloc, WeatherState>(
                        bloc: _weatherBloc,
                        builder: (context, state) {
                          final _iconColor = theme.brightness == Brightness.light ? HexColor("405EB9") : Colors.white;
                          if (state is GetCurrentWeatherStateSuccess) {
                            return IconButton(
                              icon: Icon(Icons.refresh, size: 24.sp, color: _iconColor),
                              onPressed: _onRefresh,
                            );
                          }
                          return Padding(
                            padding: EdgeInsets.only(right: 12),
                            child: SizedBox(
                                width: 16.sp,
                                height: 16.sp,
                                child: CircularProgressIndicator(strokeWidth: 3, color: _iconColor)),
                          );
                        },
                      )
                    ],
                  ),
                  SizedBox(height: 12),
                  Hero(
                    tag: "search-weather",
                    child: MaterialButton(
                      onPressed: () => _moveToSearch(context),
                      padding: EdgeInsets.zero,
                      child: SearchInputWidget(
                        enabled: false,
                        controller: _searchController,
                        placeholderText: Language.shared.searchInput,
                      ),
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      physics: NeverScrollableScrollPhysics(),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: BlocBuilder<WeatherBloc, WeatherState>(
                          bloc: _weatherBloc,
                          builder: (BuildContext context, state) {
                            if (state is GetCurrentWeatherStateSuccess) {
                              return Hero(
                                tag: "detail-screen",
                                child: MaterialButton(
                                  onPressed: () => _moveToDetails(context, state.currentWeatherModel),
                                  padding: EdgeInsets.zero,
                                  child: ItemWeatherLocation(
                                    locationModel: state.currentWeatherModel.location,
                                    currentModel: state.currentWeatherModel.current,
                                  ),
                                ),
                              );
                            }
                            return ShimmerLoading();
                          },
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )),
          ),
        ),
      ),
    );
  }
}
