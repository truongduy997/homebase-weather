import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TitleLocation extends StatelessWidget {
  const TitleLocation({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 10),
          child: AutoSizeText(
            "Malang, Indonesia",
            style: TextStyle(fontSize: 24.sp, color: Colors.white),
          ),
        ),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.only(top: 3),
          child: AutoSizeText(
            "Today, 30 Sept 11:00 AM",
            style: TextStyle(fontSize: 12.sp, color: Colors.white),
          ),
        )
      ],
    );
  }
}
