import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather/services/models/current_model.dart';
import 'package:weather/services/models/location_model.dart';
import 'package:weather/utils/hex_color.dart';
import 'package:weather/utils/language.dart';
import 'package:weather/utils/widgets/row_text_icon.dart';

class ItemWeatherLocation extends StatelessWidget {
  final LocationModel? locationModel;
  final CurrentModel? currentModel;

  const ItemWeatherLocation({super.key, this.locationModel, this.currentModel});

  @override
  Widget build(BuildContext context) {
    final _titlePosition = this.locationModel?.name ?? "";
    final _conditionText = this.currentModel?.condition?.text ?? "";
    final _iconCondition =
        (this.currentModel?.condition?.icon ?? "").replaceFirst("//", "https://");
    final _lastUpdate = this.currentModel?.lastUpdated ?? "";
    final _tempC = this.currentModel?.tempC?.toInt() ?? 0;
    final _tempCString = _tempC.toString() + "\u2103";
    final _tempF = this.currentModel?.tempF?.toInt() ?? 0;
    final _tempFString = _tempF.toString() + "\u2109";
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 112,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          gradient: LinearGradient(begin: Alignment.centerLeft, colors: [
            HexColor("405EB9").withOpacity(0.7),
            HexColor("405EB9").withOpacity(0.9)
            // Color(0xff1f005c),
          ])),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RowTextIcon(
                  title: _titlePosition,
                  iconPosition: IconPosition.left,
                  textStyle: Theme.of(context)
                      .textTheme
                      .bodyLarge
                      ?.copyWith(color: Colors.white, fontWeight: FontWeight.w500),
                  iconWidget: Icon(
                    Icons.location_pin,
                    color: Colors.white,
                    size: 18.sp,
                  ),
                ),
                SizedBox(height: 5),
                RowTextIcon(
                  title: _conditionText,
                  urlIcon: _iconCondition,
                  iconPosition: IconPosition.left,
                  textStyle: Theme.of(context).textTheme.bodySmall?.copyWith(color: Colors.white),
                  iconSize: 20.sp,
                ),
                SizedBox(height: 5),
                Padding(
                  padding: EdgeInsets.only(left: 3),
                  child: RowTextIcon(
                    title: Language.shared.lastUpdate + " " + _lastUpdate,
                    iconPosition: IconPosition.left,
                    textStyle: Theme.of(context).textTheme.bodySmall?.copyWith(color: Colors.white),
                    iconWidget: Icon(
                      Icons.update,
                      color: Colors.white,
                      size: 18.sp,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Material(
            type: MaterialType.transparency,
            child: AutoSizeText(
              "${_tempCString} / ${_tempFString}",
              style: Theme.of(context).textTheme.labelSmall?.copyWith(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
