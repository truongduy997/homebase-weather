import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class LocalizationState extends Equatable {}

class LocalizationLoading extends LocalizationState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class LocalizationInitial extends LocalizationState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class LocalizationChangedState extends LocalizationState {
  final Locale locale;

  LocalizationChangedState(this.locale);

  @override
  // TODO: implement props
  List<Object?> get props => [locale];
}
