import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/bloc/localization/localization_event.dart';
import 'package:weather/bloc/localization/localization_state.dart';

class LocalizationBloc extends Bloc<LocalizationEvent, LocalizationState> {
  LocalizationBloc() : super(LocalizationInitial()) {
    on<ChangeLanguageEvent>(_changeLanguage);
  }

  //create instance of LocalizationBloc
  static LocalizationBloc of(context) => BlocProvider.of<LocalizationBloc>(context);

  Future<void> _changeLanguage(ChangeLanguageEvent event, Emitter<LocalizationState> emitter) async {
    emitter(LocalizationLoading());
    emitter(LocalizationChangedState(event.locale));
  }
}
