import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class LocalizationEvent extends Equatable {}

class ChangeLanguageEvent extends LocalizationEvent {
  final Locale locale;

  ChangeLanguageEvent(this.locale);

  @override
  // TODO: implement props
  List<Object?> get props => [locale];
}