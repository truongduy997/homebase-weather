import 'package:flutter_bloc/flutter_bloc.dart';

import 'theme_event.dart';
import 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc() : super(LightThemeState()) {
    on<ToggleThemeEvent>(_toggleTheme);
  }

  //create instance of ThemeBloc
  static ThemeBloc of(context) => BlocProvider.of<ThemeBloc>(context);

  Future<void> _toggleTheme(ThemeEvent event, Emitter<ThemeState> emitter) async {
    if (state is LightThemeState) {
      return emitter(DarkThemeState());
    } else {
      return emitter(LightThemeState());
    }
  }
}
