import 'package:equatable/equatable.dart';

abstract class ThemeState extends Equatable {
  @override
  List<Object?> get props => [];
}

class LightThemeState extends ThemeState {}

class DarkThemeState extends ThemeState {}
