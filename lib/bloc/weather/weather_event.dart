import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather/services/models/location_model.dart';

abstract class WeatherEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class RefreshWeatherEvent extends WeatherEvent {
  @override
  List<Object?> get props => [];
}

class GetCurrentWeather extends WeatherEvent {
  final double latitude;
  final double longitude;

  GetCurrentWeather(this.latitude, this.longitude);

  @override
  List<Object?> get props => [latitude, longitude];
}

class SearchLocationWeather extends WeatherEvent {
  final String searchText;

  SearchLocationWeather(this.searchText);

  @override
  List<Object?> get props => [searchText];
}

class GetForecastWeather extends WeatherEvent {
  final LocationModel locationModel;

  GetForecastWeather(this.locationModel);

  @override
  List<Object?> get props => [locationModel];
}
