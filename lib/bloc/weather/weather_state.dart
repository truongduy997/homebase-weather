import 'package:equatable/equatable.dart';
import 'package:weather/services/models/current_weather_model.dart';
import 'package:weather/services/models/forecast_weather_model.dart';
import 'package:weather/services/models/search_location_model.dart';

abstract class WeatherState extends Equatable {
  @override
  List<Object?> get props => [];
}

class WeatherStateUnknown extends WeatherState {}

class WeatherStateLoading extends WeatherState {}

class GetCurrentWeatherStateSuccess extends WeatherState {
  final CurrentWeatherModel currentWeatherModel;

  GetCurrentWeatherStateSuccess(this.currentWeatherModel);

  @override
  List<Object?> get props => [currentWeatherModel];

  @override
  String toString() {
    // TODO: implement toString
    return "GetCurrentWeatherStateSuccess" + props.toString();
  }
}

class GetCurrentWeatherStateFailed extends WeatherState {}

class SearchLocationWeatherSuccess extends WeatherState {
  final List<SearchLocationModel> locations;

  SearchLocationWeatherSuccess(this.locations);

  @override
  List<Object?> get props => [locations];

  @override
  String toString() {
    // TODO: implement toString
    return "SearchLocationWeatherSuccess" + props.toString();
  }
}

class SearchLocationWeatherFailed extends WeatherState {}

class GetForecastWeatherSuccess extends WeatherState {
  final ForecastWeatherModel forecastWeatherModel;

  GetForecastWeatherSuccess(this.forecastWeatherModel);

  @override
  List<Object?> get props => [forecastWeatherModel];

  @override
  String toString() {
    // TODO: implement toString
    return "GetForecastWeatherSuccess" + props.toString();
  }
}

class GetForecastWeatherFailed extends WeatherState {}
