import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/bloc/weather/weather_event.dart';
import 'package:weather/bloc/weather/weather_state.dart';
import 'package:weather/services/repository.dart';
import 'package:weather/utils/functions.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherBloc() : super(WeatherStateUnknown()) {
    on<RefreshWeatherEvent>(_refresh);
    on<GetCurrentWeather>(_getCurrentWeather);
    on<SearchLocationWeather>(_searchLocationWeather);
    on<GetForecastWeather>(_getForecastWeather);
  }

  static WeatherBloc of(context) => BlocProvider.of<WeatherBloc>(context);

  void _refresh(RefreshWeatherEvent event, Emitter<WeatherState> emitter) async {
    emitter(WeatherStateUnknown());
  }

  void _getCurrentWeather(GetCurrentWeather event, Emitter<WeatherState> emitter) async {
    emitter(WeatherStateLoading());
    final latLng = "${event.latitude},${event.longitude}";
    try {
      final response = await Repository.shared.getCurrentWeather(latLng, "vi");
      emitter(GetCurrentWeatherStateSuccess(response));
    } catch (error) {
      printErrorMessage("GetCurrentWeatherStateFailed", error);
      emitter(GetCurrentWeatherStateFailed());
    }
  }

  void _searchLocationWeather(SearchLocationWeather event, Emitter<WeatherState> emitter) async {
    emitter(WeatherStateLoading());
    try {
      final response = await Repository.shared.searchLocation(event.searchText, "vi");
      emitter(SearchLocationWeatherSuccess(response));
    } catch (error) {
      printErrorMessage("SearchLocationWeatherFailed", error);
      emitter(SearchLocationWeatherFailed());
    }
  }

  void _getForecastWeather(GetForecastWeather event, Emitter<WeatherState> emitter) async {
    emitter(WeatherStateLoading());
    final latLng = "${event.locationModel.lat},${event.locationModel.lon}";
    try {
      final response = await Repository.shared.getForecastWeather(latLng, "vi", 5);
      emitter(GetForecastWeatherSuccess(response));
    } catch (error) {
      printErrorMessage("GetForecastWeatherFailed", error);
      emitter(GetForecastWeatherFailed());
    }
  }
}
