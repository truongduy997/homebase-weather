import 'package:get_it/get_it.dart';
import 'package:weather/navigator/navigator_service.dart';

class Singleton {
  static final _shared = GetIt.instance;
  static void setupLocators() {
    // register Navigation service locator
    // _shared.registerSingleton<RestClient>(RestClient(DioClient.shared.initialization()));
    _shared.registerSingleton<NavigatorService>(NavigatorService());
  }

  static void dispose() {
    _shared.reset();
  }

  // create singleton for DioClient
  // static RestClient restClient() {
  //   return _shared<RestClient>();
  // }

  // create singleton for NavigatorService
  static NavigatorService navigatorService() {
    return _shared<NavigatorService>();
  }
}
