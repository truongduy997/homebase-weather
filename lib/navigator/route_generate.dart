import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:weather/navigator/screens_name.dart';
import 'package:weather/screens/details/details.dart';
import 'package:weather/screens/home/home.dart';
import 'package:weather/screens/search/search.dart';
import 'package:weather/services/models/current_weather_model.dart';

MaterialPageRoute routeGenerate(RouteSettings settings) {
  final args = settings.arguments;
  switch (settings.name) {
    case ScreensName.home:
      return MaterialPageRoute(builder: (_) => const HomeScreen(), settings: RouteSettings(name: settings.name));
    case ScreensName.details:
      return MaterialPageRoute(
          builder: (_) => DetailsScreen(currentWeatherModel: args as CurrentWeatherModel), settings: RouteSettings(name: settings.name));
    case ScreensName.search:
      return MaterialPageRoute(builder: (_) => SearchScreen(), settings: RouteSettings(name: settings.name));
    default:
      return MaterialPageRoute(builder: (_) => const ErrorScreen());
  }
}

class ErrorScreen extends StatefulWidget {
  const ErrorScreen({super.key});

  @override
  State<ErrorScreen> createState() => _ErrorScreenState();
}

class _ErrorScreenState extends State<ErrorScreen> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.no_encryption_gmailerrorred_outlined, size: 100),
            AutoSizeText("Đã có lỗi xảy ra")
          ],
        ),
      ),
    );
  }
}
