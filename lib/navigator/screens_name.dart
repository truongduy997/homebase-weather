class ScreensName {
  static const splash = '/';
  static const home = '/home';
  static const details = '/details';
  static const search = '/search';
  static const error = '/error';
}
