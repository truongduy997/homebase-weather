import 'package:weather/services/dio_client.dart';
import 'package:weather/services/models/current_weather_model.dart';
import 'package:weather/services/models/forecast_weather_model.dart';
import 'package:weather/services/models/search_location_model.dart';
import 'package:weather/services/rest/rest_client.dart';
import 'package:weather/utils/functions.dart';

class Repository {
  Repository._internal();

  static final Repository shared = Repository._internal();

  factory Repository() => shared;
  final _dio = DioClient.instance();

  //
  Future<CurrentWeatherModel> getCurrentWeather(String latLng, String lang) {
    final client = RestClient(_dio);
    return client.getCurrentWeather(latLng, lang).then((response) {
      return response;
    }).catchError((error) {
      printErrorMessage("getCurrentWeather", error);
      return CurrentWeatherModel();
    });
  }

  Future<ForecastWeatherModel> getForecastWeather(String latLng, String lang, int days) {
    final client = RestClient(_dio);
    return client.getForecastWeather(latLng, lang, days).then((response) {
      return response;
    }).catchError((error) {
      printErrorMessage("getForecastWeather", error);
      return ForecastWeatherModel();
    });
  }

  Future<List<SearchLocationModel>> searchLocation(String searchText, String lang) {
    final client = RestClient(_dio);
    return client.searchWeather(searchText, lang).then((response) {
      return response;
    }).catchError((error) {
      printErrorMessage("getForecastWeather", error);
      return <SearchLocationModel>[];
    });
  }
}
