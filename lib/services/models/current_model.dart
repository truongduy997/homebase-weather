import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:weather/services/models/condition_model.dart';

part 'current_model.g.dart';

@JsonSerializable()
class CurrentModel extends Equatable {
  @JsonKey(name: "last_updated_epoch")
  final int? lastUpdatedEpoch;
  @JsonKey(name: "last_updated")
  final String? lastUpdated;
  @JsonKey(name: "temp_c")
  final double? tempC;
  @JsonKey(name: "temp_f")
  final double? tempF;
  @JsonKey(name: "is_day")
  final int? isDay;
  final ConditionModel? condition;
  @JsonKey(name: "wind_mph")
  final double? windMph;
  @JsonKey(name: "wind_kph")
  final double? windKph;
  @JsonKey(name: "wind_degree")
  final int? windDegree;
  @JsonKey(name: "wind_dir")
  final String? windDir;
  @JsonKey(name: "pressure_mb")
  final double? pressureMb;
  @JsonKey(name: "pressure_in")
  final double? pressureIn;
  @JsonKey(name: "precip_mm")
  final double? precipMm;
  @JsonKey(name: "precip_in")
  final double? precipIn;
  final int? humidity;
  final int? cloud;
  @JsonKey(name: "feelslike_c")
  final double? feelslikeC;
  @JsonKey(name: "feelslike_f")
  final double? feelslikeF;
  @JsonKey(name: "vis_km")
  final double? visKm;
  @JsonKey(name: "vis_miles")
  final double? visMiles;
  final double? uv;
  @JsonKey(name: "gust_mph")
  final double? gustMph;
  @JsonKey(name: "gust_kph")
  final double? gustKph;

  CurrentModel(
      {this.lastUpdatedEpoch,
      this.lastUpdated,
      this.tempC,
      this.tempF,
      this.isDay,
      this.condition,
      this.windMph,
      this.windKph,
      this.windDegree,
      this.windDir,
      this.pressureMb,
      this.pressureIn,
      this.precipMm,
      this.precipIn,
      this.humidity,
      this.cloud,
      this.feelslikeC,
      this.feelslikeF,
      this.visKm,
      this.visMiles,
      this.uv,
      this.gustMph,
      this.gustKph});

  /// factory.
  factory CurrentModel.fromJson(Map<String, dynamic> json) => _$CurrentModelFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentModelToJson(this);

  @override
  List<Object?> get props => [
        lastUpdatedEpoch,
        lastUpdated,
        tempC,
        tempF,
        isDay,
        condition,
        windMph,
        windKph,
        windDegree,
        windDir,
        pressureMb,
        pressureIn,
        precipMm,
        precipIn,
        humidity,
        cloud,
        feelslikeC,
        feelslikeF,
        visKm,
        visMiles,
        uv,
        gustMph,
        gustKph
      ];
}
