import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:weather/services/models/condition_model.dart';

part 'forecast_model.g.dart';

@JsonSerializable()
class ForecastModel extends Equatable {
  @JsonKey(name: "forecastday")
  final List<ForecastDayModel>? forecastDay;

  ForecastModel({this.forecastDay});

  /// factory.
  factory ForecastModel.fromJson(Map<String, dynamic> json) => _$ForecastModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [forecastDay];
}

@JsonSerializable()
class ForecastDayModel extends Equatable {
  final String? date;
  @JsonKey(name: "date_epoch")
  final int? dateEpoch;
  final DayModel? day;
  final AstroModel? astro;
  final List<HourModel>? hour;

  ForecastDayModel({this.date, this.dateEpoch, this.day, this.astro, this.hour});

  /// factory.
  factory ForecastDayModel.fromJson(Map<String, dynamic> json) => _$ForecastDayModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastDayModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [date, dateEpoch, day, astro, hour];
}

@JsonSerializable()
class DayModel extends Equatable {
  @JsonKey(name: "maxtemp_c")
  final double? maxtempC;
  @JsonKey(name: "maxtemp_f")
  final double? maxtempF;
  @JsonKey(name: "mintemp_c")
  final double? mintempC;
  @JsonKey(name: "mintemp_f")
  final double? mintempF;
  @JsonKey(name: "avgtemp_c")
  final double? avgtempC;
  @JsonKey(name: "avgtemp_f")
  final double? avgtempF;
  @JsonKey(name: "maxwind_mph")
  final double? maxwindMph;
  @JsonKey(name: "maxwind_kph")
  final double? maxwindKph;
  @JsonKey(name: "totalprecip_mm")
  final double? totalprecipMm;
  @JsonKey(name: "totalprecip_in")
  final double? totalprecipIn;
  @JsonKey(name: "totalsnow_cm")
  final double? totalsnowCm;
  @JsonKey(name: "avgvis_km")
  final double? avgvisKm;
  @JsonKey(name: "avgvis_miles")
  final double? avgvisMiles;
  @JsonKey(name: "avghumidity")
  final double? avghumidity;
  @JsonKey(name: "daily_will_it_rain")
  final int? dailyWillItRain;
  @JsonKey(name: "daily_chance_of_rain")
  final int? dailyChanceOfRain;
  @JsonKey(name: "daily_will_it_snow")
  final int? dailyWillItSnow;
  @JsonKey(name: "daily_chance_of_snow")
  final int? dailyChanceOfSnow;
  final ConditionModel? condition;
  final double? uv;

  DayModel(
      {this.maxtempC,
      this.maxtempF,
      this.mintempC,
      this.mintempF,
      this.avgtempC,
      this.avgtempF,
      this.maxwindMph,
      this.maxwindKph,
      this.totalprecipMm,
      this.totalprecipIn,
      this.totalsnowCm,
      this.avgvisKm,
      this.avgvisMiles,
      this.avghumidity,
      this.dailyWillItRain,
      this.dailyChanceOfRain,
      this.dailyWillItSnow,
      this.dailyChanceOfSnow,
      this.condition,
      this.uv});

  /// factory.
  factory DayModel.fromJson(Map<String, dynamic> json) => _$DayModelFromJson(json);

  Map<String, dynamic> toJson() => _$DayModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [
        maxtempC,
        maxtempF,
        mintempC,
        mintempF,
        avgtempC,
        avgtempF,
        maxwindMph,
        maxwindKph,
        totalprecipMm,
        totalprecipIn,
        totalsnowCm,
        avgvisKm,
        avgvisMiles,
        avghumidity,
        dailyWillItRain,
        dailyChanceOfRain,
        dailyWillItSnow,
        dailyChanceOfSnow,
        condition,
        uv
      ];
}

@JsonSerializable()
class AstroModel extends Equatable {
  final String? sunrise;
  final String? sunset;
  final String? moonrise;
  final String? moonset;
  @JsonKey(name: "moon_phase")
  final String? moonPhase;
  @JsonKey(name: "moon_illumination")
  final int? moonIllumination;
  @JsonKey(name: "is_moon_up")
  final int? isMoonUp;
  @JsonKey(name: "is_sun_up")
  final int? isSunUp;

  AstroModel(
      {this.sunrise,
      this.sunset,
      this.moonrise,
      this.moonset,
      this.moonPhase,
      this.moonIllumination,
      this.isMoonUp,
      this.isSunUp});

  /// factory.
  factory AstroModel.fromJson(Map<String, dynamic> json) => _$AstroModelFromJson(json);

  Map<String, dynamic> toJson() => _$AstroModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props =>
      [sunrise, sunset, moonrise, moonset, moonPhase, moonIllumination, isMoonUp, isSunUp];
}

@JsonSerializable()
class HourModel extends Equatable {
  @JsonKey(name: "time_epoch")
  final int? timeEpoch;
  final String? time;
  @JsonKey(name: "temp_c")
  final double? tempC;
  @JsonKey(name: "temp_f")
  final double? tempF;
  @JsonKey(name: "is_day")
  final int? isDay;
  final ConditionModel? condition;
  @JsonKey(name: "wind_mph")
  final double? windMph;
  @JsonKey(name: "wind_kph")
  final double? windKph;
  @JsonKey(name: "wind_degree")
  final int? windDegree;
  @JsonKey(name: "wind_dir")
  final String? windDir;
  @JsonKey(name: "pressure_mb")
  final double? pressureMb;
  @JsonKey(name: "pressure_in")
  final double? pressureIn;
  @JsonKey(name: "precip_mm")
  final double? precipMm;
  @JsonKey(name: "precip_in")
  final double? precipIn;
  final int? humidity;
  final int? cloud;
  @JsonKey(name: "feelslike_c")
  final double? feelslikeC;
  @JsonKey(name: "feelslike_f")
  final double? feelslikeF;
  @JsonKey(name: "windchill_c")
  final double? windchillC;
  @JsonKey(name: "windchill_f")
  final double? windchillF;
  @JsonKey(name: "heatindex_c")
  final double? heatindexC;
  @JsonKey(name: "heatindex_f")
  final double? heatindexF;
  @JsonKey(name: "dewpoint_c")
  final double? dewpointC;
  @JsonKey(name: "dewpoint_f")
  final double? dewpointF;
  @JsonKey(name: "will_it_rain")
  final int? willItRain;
  @JsonKey(name: "chance_of_rain")
  final int? chanceOfRain;
  @JsonKey(name: "will_it_snow")
  final int? willItSnow;
  @JsonKey(name: "chance_of_snow")
  final int? chanceOfSnow;
  @JsonKey(name: "vis_km")
  final double? visKm;
  @JsonKey(name: "vis_miles")
  final double? visMiles;
  @JsonKey(name: "gust_mph")
  final double? gustMph;
  @JsonKey(name: "gust_kph")
  final double? gustKph;
  final double? uv;

  HourModel(
      {this.timeEpoch,
      this.time,
      this.tempC,
      this.tempF,
      this.isDay,
      this.condition,
      this.windMph,
      this.windKph,
      this.windDegree,
      this.windDir,
      this.pressureMb,
      this.pressureIn,
      this.precipMm,
      this.precipIn,
      this.humidity,
      this.cloud,
      this.feelslikeC,
      this.feelslikeF,
      this.windchillC,
      this.windchillF,
      this.heatindexC,
      this.heatindexF,
      this.dewpointC,
      this.dewpointF,
      this.willItRain,
      this.chanceOfRain,
      this.willItSnow,
      this.chanceOfSnow,
      this.visKm,
      this.visMiles,
      this.gustMph,
      this.gustKph,
      this.uv});

  /// factory.
  factory HourModel.fromJson(Map<String, dynamic> json) => _$HourModelFromJson(json);

  Map<String, dynamic> toJson() => _$HourModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [
        timeEpoch,
        time,
        tempC,
        tempF,
        isDay,
        condition,
        windMph,
        windKph,
        windDegree,
        windDir,
        pressureMb,
        pressureIn,
        precipMm,
        precipIn,
        humidity,
        cloud,
        feelslikeC,
        feelslikeF,
        windchillC,
        windchillF,
        heatindexC,
        heatindexF,
        dewpointC,
        dewpointF,
        willItRain,
        chanceOfRain,
        willItSnow,
        chanceOfSnow,
        visKm,
        visMiles,
        gustMph,
        gustKph,
        uv
      ];
}
