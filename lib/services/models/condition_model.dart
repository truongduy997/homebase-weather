import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'condition_model.g.dart';

@JsonSerializable()
class ConditionModel extends Equatable {
  final String? text;
  final String? icon;
  final int? code;

  ConditionModel({this.text, this.icon, this.code});

  /// factory.
  factory ConditionModel.fromJson(Map<String, dynamic> json) => _$ConditionModelFromJson(json);

  Map<String, dynamic> toJson() => _$ConditionModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [text, icon, code];
}
