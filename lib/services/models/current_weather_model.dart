import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:weather/services/models/current_model.dart';
import 'package:weather/services/models/location_model.dart';

part 'current_weather_model.g.dart';

@JsonSerializable()
class CurrentWeatherModel extends Equatable {
  final LocationModel? location;
  final CurrentModel? current;

  CurrentWeatherModel({this.location, this.current});

  /// factory.
  factory CurrentWeatherModel.fromJson(Map<String, dynamic> json) =>
      _$CurrentWeatherModelFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentWeatherModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [location, current];
}
