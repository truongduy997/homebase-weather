import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:weather/services/models/current_model.dart';
import 'package:weather/services/models/forecast_model.dart';
import 'package:weather/services/models/location_model.dart';

part 'forecast_weather_model.g.dart';

@JsonSerializable()
class ForecastWeatherModel extends Equatable {
  final LocationModel? location;
  final CurrentModel? current;
  final ForecastModel? forecast;

  ForecastWeatherModel({this.location, this.current, this.forecast});

  /// factory.
  factory ForecastWeatherModel.fromJson(Map<String, dynamic> json) =>
      _$ForecastWeatherModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForecastWeatherModelToJson(this);

  @override
  // TODO: implement props
  List<Object?> get props => [location, current, forecast];
}
