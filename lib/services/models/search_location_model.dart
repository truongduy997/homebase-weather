import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'search_location_model.g.dart';

@JsonSerializable()
class SearchLocationModel extends Equatable {
  final int? id;
  final String? name;
  final String? region;
  final String? country;
  final double? lat;
  final double? lon;
  final String? url;

  SearchLocationModel(
      {this.id, this.name, this.region, this.country, this.lat, this.lon, this.url});

  /// factory.
  factory SearchLocationModel.fromJson(Map<String, dynamic> json) =>
      _$SearchLocationModelFromJson(json);

  Map<String, dynamic> toJson() => _$SearchLocationModelToJson(this);

  @override
  List<Object?> get props => [id, name, region, country, lat, lon, url];
}
