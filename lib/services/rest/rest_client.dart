import 'dart:async';

import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:weather/services/models/current_weather_model.dart';
import 'package:weather/services/models/forecast_weather_model.dart';
import 'package:weather/services/models/search_location_model.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: 'http://api.weatherapi.com/v1')
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/current.json")
  Future<CurrentWeatherModel> getCurrentWeather(
    @Query('q') String q,
    @Query('lang') String lang,
  );

  @GET("/forecast.json")
  Future<ForecastWeatherModel> getForecastWeather(
    @Query('q') String q,
    @Query('lang') String lang,
    @Query('days') int days,
  );

  @GET("/search.json")
  Future<List<SearchLocationModel>> searchWeather(
    @Query('q') String q,
    @Query('lang') String lang,
  );
}
