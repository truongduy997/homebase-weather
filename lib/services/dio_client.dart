import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:weather/utils/functions.dart';
import 'package:weather/utils/widgets/alert_widget.dart';

const String keyWeather = "d2124fdc57d84a1f8b4152540232310";

class DioClient {
  static Dio instance() {
    final dio = Dio();
    dio.interceptors.add(HttpLogInterceptor());
    return dio;
  }
}

class HttpLogInterceptor extends InterceptorsWrapper {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    options.cancelToken = CancelToken();
    // TODO: implement onRequest
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none && AlertWidget.isCurrentAlert == false) {
      AlertWidget.alertDefault(
          title: "Kết nối", desc: "Không có kết nối internet, vui lòng kiểm tra mạng.");
      options.cancelToken?.cancel();
    }
    options.headers.putIfAbsent("Content-Type", () => "application/json");
    options.queryParameters.putIfAbsent("key", () => keyWeather);
    printInfoMessage('onRequest: ${options.uri}\n'
        'data=${options.data}\n'
        'method=${options.method}\n'
        'headers=${options.headers}\n'
        'queryParameters=${options.queryParameters}');
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    // TODO: implement onResponse
    printInfoMessage("Response => ${response.requestOptions.uri})\n${jsonEncode(response.data)}");
    super.onResponse(response, handler);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) async {
    // TODO: implement onError
    printErrorMessage("Request Error => ${err.message}", err);
    super.onError(err, handler);
  }
}
