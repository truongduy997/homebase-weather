import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:weather/di/singleton.dart';

class AlertWidget {
  static bool isCurrentAlert = false;

  static void alertDefault(
      {Function? onPress,
      String title = 'FLUTTER ALERT',
      String desc = 'Flutter is more awesome with RFlutter Alert.'}) {
    final BuildContext context = Singleton.navigatorService().currentContext();
    AlertWidget.isCurrentAlert = true;

    void onDismissFunc() {
      onPress ?? Navigator.pop(context);
      AlertWidget.isCurrentAlert = false;
    }

    Alert(
      context: context,
      type: AlertType.none,
      style: AlertStyle(
          animationType: AnimationType.grow,
          titleStyle: const TextStyle(fontSize: 20),
          backgroundColor: Theme.of(context).colorScheme.background,
          descStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
          alertPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 24.0),
          animationDuration: const Duration(milliseconds: 350)),
      content: Column(
        children: [
          Center(
            child: AutoSizeText(title,
                style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Center(
              child: AutoSizeText(desc,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                  textAlign: TextAlign.center),
            ),
          )
        ],
      ),
      buttons: [
        DialogButton(
            radius: BorderRadius.circular(30),
            width: MediaQuery.of(context).size.width - 100,
            height: 44,
            // color: Theme.of(context).buttonColor,
            onPressed: onDismissFunc,
            child: const AutoSizeText(
              'Đồng ý',
              style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.w500),
            )),
      ],
    ).show();
  }

  static void alertUpgradeVersion(
      {Function(BuildContext context)? onPress,
      String title = 'FLUTTER ALERT',
      String desc = 'Flutter is more awesome with RFlutter Alert.'}) {
    AlertWidget.isCurrentAlert = true;
    final BuildContext context = Singleton.navigatorService().currentContext();

    void onDismissFunc() {
      Navigator.pop(context);
      AlertWidget.isCurrentAlert = false;
    }

    void onPressFunc() {
      if (onPress != null) {
        onPress(context);
      } else {
        onDismissFunc();
      }
    }

    Alert(
      context: context,
      type: AlertType.none,
      style: AlertStyle(
          animationType: AnimationType.grow,
          titleStyle: const TextStyle(fontSize: 20),
          backgroundColor: Theme.of(context).colorScheme.background,
          descStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
          alertPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 24.0),
          animationDuration: const Duration(milliseconds: 350)),
      content: Column(
        children: [
          Center(
            child: AutoSizeText(title,
                style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Center(
              child: AutoSizeText(desc,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                  textAlign: TextAlign.center),
            ),
          )
        ],
      ),
      buttons: [
        DialogButton(
            radius: BorderRadius.circular(30),
            width: MediaQuery.of(context).size.width - 100,
            height: 44,
            // color: Theme.of(context).buttonColor.withAlpha(180),
            onPressed: onDismissFunc,
            child: const AutoSizeText(
              'Bỏ qua',
              style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.w500),
            )),
        DialogButton(
            radius: BorderRadius.circular(30),
            width: MediaQuery.of(context).size.width - 100,
            height: 44,
            // color: Theme.of(context).buttonColor.withAlpha(255),
            onPressed: onPressFunc,
            child: const AutoSizeText(
              'Đồng ý',
              style: TextStyle(fontSize: 16, color: Colors.white, fontWeight: FontWeight.w500),
            )),
      ],
    ).show();
  }
}
