//create class ButtonRadius extends StatelessWidget
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class ButtonRadius extends StatelessWidget {
  //create const ButtonRadius
  const ButtonRadius({
    super.key,
    required this.onTap,
    required this.text,
    this.color = Colors.black12,
    this.textColor = Colors.black45,
    this.fontSize = 16,
    this.fontWeight = FontWeight.bold,
    this.height = 44,
    this.width = double.infinity,
    this.borderRadius = 22,
  });

  //create final onTap
  final VoidCallback onTap;

  //create final text
  final String text;

  //create final color
  final Color color;

  //create final textColor
  final Color textColor;

  //create final fontSize
  final double fontSize;

  //create final fontWeight
  final FontWeight fontWeight;

  //create final height
  final double height;

  //create final width
  final double width;

  //create final borderRadius
  final double borderRadius;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: AutoSizeText(
            text,
            style: TextStyle(color: textColor, fontWeight: fontWeight, fontSize: fontSize),
          ),
        ),
      ),
    );
  }
}
