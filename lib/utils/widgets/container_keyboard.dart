import 'package:flutter/material.dart';

class ContainerKeyboard extends StatelessWidget {
  const ContainerKeyboard({super.key, required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () => FocusScope.of(context).unfocus(),
      child: child,
    );
  }
}
