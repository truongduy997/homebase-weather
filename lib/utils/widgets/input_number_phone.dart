// create class InputNumberPhone extends StatefulWidget

import 'package:flutter/material.dart';
import 'package:weather/utils/language.dart';

class InputNumberPhone extends StatefulWidget {
  final TextEditingController controller;

  const InputNumberPhone({super.key, required this.controller});

  @override
  State<InputNumberPhone> createState() => _InputNumberPhoneState();
}

class _InputNumberPhoneState extends State<InputNumberPhone> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      keyboardType: TextInputType.phone,
      maxLength: 10,
      decoration: InputDecoration(
          border: const UnderlineInputBorder(), labelText: Language.shared.numberPhone),
    );
  }
}
