import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather/utils/functions.dart';

enum IconPosition { left, right }

class RowTextIcon extends StatelessWidget {
  final String? urlIcon;
  final String title;
  final IconPosition? iconPosition;
  final double? iconSize;
  final TextStyle? textStyle;
  final Widget? iconWidget;
  final TextAlign? textAlign;
  final int maxLines;

  const RowTextIcon(
      {super.key,
      this.urlIcon,
      required this.title,
      this.iconPosition = IconPosition.left,
      this.textStyle,
      this.iconSize,
      this.iconWidget,
      this.textAlign,
      this.maxLines = 1});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        iconPosition == IconPosition.left
            ? iconWidget ??
                CachedNetworkImage(
                  width: iconSize ?? 20.sp,
                  height: iconSize ?? 20.sp,
                  imageUrl: urlIcon ?? "",
                  cacheKey: urlIcon,
                  placeholder: (context, url) {
                    return SizedBox(
                        width: 16.sp,
                        height: 16.sp,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                        ));
                  },
                  errorWidget: (context, url, error) {
                    printErrorMessage("CachedNetworkImage", error);
                    return Icon(
                      Icons.error,
                      size: 18.sp,
                      color: Colors.white,
                    );
                  },
                )
            : SizedBox(),
        SizedBox(
          width: iconPosition == IconPosition.left ? 10 : 0,
        ),
        Expanded(
          child: Material(
            type: MaterialType.transparency,
            child: AutoSizeText(
              title,
              maxLines: this.maxLines,
              textAlign: textAlign,
              overflow: TextOverflow.ellipsis,
              style: textStyle ?? Theme.of(context).textTheme.bodyMedium?.copyWith(color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          width: iconPosition == IconPosition.right ? 10 : 0,
        ),
        iconPosition == IconPosition.right
            ? iconWidget ??
                CachedNetworkImage(
                  width: iconSize ?? 20.sp,
                  height: iconSize ?? 20.sp,
                  imageUrl: urlIcon ?? "",
                  cacheKey: urlIcon,
                  placeholder: (context, url) {
                    return SizedBox(
                        width: 16.sp,
                        height: 16.sp,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                        ));
                  },
                  errorWidget: (context, url, error) {
                    printErrorMessage("CachedNetworkImage", error);
                    return Icon(
                      Icons.error,
                      size: 18.sp,
                      color: Colors.white,
                    );
                  },
                )
            : SizedBox(),
      ],
    );
  }
}
