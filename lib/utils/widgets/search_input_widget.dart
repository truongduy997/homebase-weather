import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SearchInputWidget extends StatefulWidget {
  final TextEditingController controller;
  final Function(String)? onChangedText;
  final String placeholderText;
  final bool enabled;
  final FocusNode? focusNode;

  const SearchInputWidget({
    Key? key,
    required this.controller,
    this.enabled = true,
    this.onChangedText = null,
    this.placeholderText = "",
    this.focusNode,
  }) : super(key: key);

  @override
  SearchInputWidgetState createState() => SearchInputWidgetState();
}

class SearchInputWidgetState extends State<SearchInputWidget> {
  final StreamController _streamController = StreamController();

  void _onChangeText(String text) {
    _streamController.sink.add(text.length > 0);
    if (widget.onChangedText != null) {
      widget.onChangedText!(text);
    }
  }

  void _clearText() {
    widget.controller.clear();
    _streamController.sink.add(false);
    if (widget.onChangedText != null) {
      widget.onChangedText!("");
    }
  }

  @override
  void dispose() {
    super.dispose();
    _streamController.close();
    // widget.controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool isDarkMode = Theme.of(context).brightness == Brightness.dark;
    return Container(
      height: 48,
      child: TextField(
        enabled: widget.enabled,
        controller: widget.controller,
        style: TextStyle(fontSize: 14.sp),
        textAlign: TextAlign.start,
        maxLines: 1,
        focusNode: widget.focusNode,
        onChanged: _onChangeText,
        textInputAction: TextInputAction.search,
        decoration: InputDecoration(
          filled: true,
          suffixIcon: StreamBuilder(
            stream: _streamController.stream,
            builder: (context, snapShot) {
              if (snapShot.hasData && snapShot.data == true) {
                return IconButton(onPressed: _clearText, icon: const Icon(Icons.clear));
              }
              return const SizedBox();
            },
          ),
          fillColor: isDarkMode ? Colors.blueGrey.shade900 : Colors.blueGrey.shade50,
          prefixIcon: const Icon(Icons.search, size: 20),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(24), borderSide: BorderSide.none),
          isDense: true,
          contentPadding: const EdgeInsets.all(0),
          hintText: widget.placeholderText,
          // hintStyle: TextStyle()
        ),
      ),
    );
  }
}
