import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:weather/utils/asset/colors.dart';

class ShimmerLoading extends StatelessWidget {
  final double width;
  final double height;
  final double borderRadius;
  final Color? baseColor;
  final Color? highlightColor;

  ShimmerLoading(
      {super.key,
      this.width = double.infinity,
      this.height = 100,
      this.borderRadius = 15,
      this.baseColor,
      this.highlightColor});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: this.width,
        height: this.height,
        child: Shimmer.fromColors(
          period: Duration(milliseconds: 1000),
          baseColor: baseColor ?? AssetColors.background,
          highlightColor: highlightColor ?? Theme.of(context).primaryColor,
          enabled: true,
          child: Container(
            decoration: BoxDecoration(
              color: baseColor ?? AssetColors.background,
              borderRadius: BorderRadius.circular(this.borderRadius),
            ),
          ),
        ));
  }
}
