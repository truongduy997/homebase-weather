import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// Define ThemeData for light mode
ThemeData _lightTheme = ThemeData(
  // Define the primary color
  primaryColor: Colors.blueAccent,
  // Define the secondary color
  colorScheme: const ColorScheme(
    brightness: Brightness.light,
    primary: Colors.blue,
    secondary: Colors.green,
    surface: Colors.white,
    background: Colors.white,
    error: Colors.red,
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.black,
    onBackground: Colors.black,
    onError: Colors.white,
    // Define the default brightness and colors
  ),
  // Define the text color
  textTheme: TextTheme(
    bodySmall: TextStyle(color: Colors.black, fontSize: 12.sp),
    bodyLarge: TextStyle(color: Colors.black, fontSize: 16.sp),
    bodyMedium: TextStyle(color: Colors.black, fontSize: 14.sp),
    titleSmall: TextStyle(color: Colors.black, fontSize: 24.sp),
    titleLarge: TextStyle(color: Colors.black, fontSize: 32.sp),
    titleMedium: TextStyle(color: Colors.black, fontSize: 28.sp),
    labelSmall: TextStyle(color: Colors.black, fontSize: 18.sp),
    labelLarge: TextStyle(color: Colors.black, fontSize: 28.sp),
    labelMedium: TextStyle(color: Colors.black, fontSize: 24.sp),
  ),
);

// Define ThemeData for dark mode
ThemeData _darkTheme = ThemeData(
// Define the primary color
  primaryColor: Colors.blueAccent,
// Define the secondary color
  colorScheme: const ColorScheme(
    brightness: Brightness.dark,
    primary: Colors.blue,
    secondary: Colors.green,
    surface: Colors.black,
    background: Colors.black,
    error: Colors.red,
    onPrimary: Colors.white,
    onSecondary: Colors.white,
    onSurface: Colors.white,
    onBackground: Colors.white,
    onError: Colors.white,
// Define the default brightness and colors
  ),
// Define the text color
  textTheme: TextTheme(
    bodySmall: TextStyle(color: Colors.white, fontSize: 12.sp),
    bodyLarge: TextStyle(color: Colors.white, fontSize: 16.sp),
    bodyMedium: TextStyle(color: Colors.white, fontSize: 14.sp),
    titleSmall: TextStyle(color: Colors.white, fontSize: 24.sp),
    titleLarge: TextStyle(color: Colors.white, fontSize: 32.sp),
    titleMedium: TextStyle(color: Colors.white, fontSize: 28.sp),
    labelSmall: TextStyle(color: Colors.white, fontSize: 18.sp),
    labelLarge: TextStyle(color: Colors.white, fontSize: 28.sp),
    labelMedium: TextStyle(color: Colors.white, fontSize: 24.sp),
  ),
);

ThemeData getThemeData(Brightness brightness) {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: brightness == Brightness.light ? Colors.transparent : Colors.transparent,
    // Set status bar color based on dark mode status
    statusBarBrightness: brightness, // Set status bar brightness based on dark mode status
  ));
  if (brightness == Brightness.light) {
    return _lightTheme;
  } else {
    return _darkTheme;
  }
}
