import 'dart:async';

import 'package:logger/logger.dart';

extension NumberExtension on num {
  bool get isInteger => this is int;

  bool get isDouble => this is double;
}

Timer? _debounce;

void debounceFunc(Function()? onCompleted, int? milliseconds) {
  if (_debounce != null) {
    _debounce?.cancel();
  }
  _debounce = Timer(Duration(milliseconds: milliseconds ?? 500), () {
    if (onCompleted != null) onCompleted();
  });
}

final logger = Logger(
  printer: PrettyPrinter(
      methodCount: 2, // Number of method calls to be displayed
      errorMethodCount: 8, // Number of method calls if stacktrace is provided
      lineLength: 120, // Width of the output
      colors: true, // Colorful log messages
      printEmojis: true, // Print an emoji for each log message
      printTime: false // Should each log print contain a timestamp
      ),
);

void printMessage(dynamic message) => logger.t(message);
void printDebugMessage(dynamic message) => logger.d(message);
void printInfoMessage(dynamic message) => logger.d(message);
void printWarningMessage(dynamic message) => logger.d(message);
void printErrorMessage(dynamic message, dynamic e) => logger.e(message, error: e);

int compareTimeMinutes(int pastTime) {
  final now = DateTime.now();
  final past = DateTime.fromMicrosecondsSinceEpoch(pastTime);
  return now.difference(past).inMinutes;
}

// create functions validate number phone Vietnam
bool validatePhoneNumber(String value) {
  final regex = RegExp(r"^((\+84)|(84)|(0))[0-9]{9}$");
  return regex.hasMatch(value);
}
