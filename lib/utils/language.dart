import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:weather/di/singleton.dart';

class Language {
  Language._privateConstructor();
  static final _instance = Language._privateConstructor();
  factory Language() => _instance;
  AppLocalizations _appLocalizations() {
    return AppLocalizations.of(Singleton.navigatorService().currentContext())!;
  }

  static AppLocalizations get shared => _instance._appLocalizations();
}
