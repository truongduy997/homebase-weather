import 'dart:ui';

import 'package:weather/utils/hex_color.dart';

class AssetColors {
  static Color background = HexColor("#59C1E1");
}
