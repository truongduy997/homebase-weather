# Weather App (Demo project for Homebase)

Objective: Create a basic Flutter mobile app that fetches and displays current weather information for a given location.

**Requirements**:
1. Location Input: Users should be able to input a location (e.g., city name or zip code).
2. Weather Display: Upon entering a location, the app should fetch and display the current weather information, including:

    ● Temperature (in Celsius or Fahrenheit)

    ● Weather condition (e.g., sunny, cloudy, rainy)

3. Error Handling: Implement basic error handling for cases where the location input is invalid or the weather data cannot be retrieved.
4. User-Friendly UI: Design a simple and user-friendly UI.


## Getting started

* Features included in the app:
    * Api using for get data `https://www.weatherapi.com/docs/`
    * Flutter BloC `flutter_bloc`
    * Fetch data using `Dio, Retrofit`
    * DTO using `json_serializable`, `json_annotation`
    * Single class `get_it`
    * Auto generate asset images `flutter gen`
    * Dark Mode `animated_theme_switcher`
    * Multiple languages `flutter_localizations`
    * Home: Auto get current location and show one row detail weather
    * Search: Enter name city or LatLng `48.8567,2.3508`
    * Details: Show more than information about location and have day of week (5 days)

* How to run:
  * a. `flutter pub get`
  * b. `flutter pub run build_runner build --delete-conflicting-outputs`
  * c. `flutter devices` command to verify that Flutter recognizes your connected Android device. 
  * d. Start your app by running `flutter run`.
    
